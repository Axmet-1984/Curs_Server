﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace Curs_Server
{
    public class PacientContext : DbContext
    {
        public PacientContext() : base("connstringPacientDb")
        {

        }

        public DbSet<Doc_Name> DocNames { get; set; }
        public DbSet<Doc_Profile> DocProfiles { get; set; }
        public DbSet<Pacients> Pacients { get; set; }

    }
}
