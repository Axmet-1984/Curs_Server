﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Curs_Server
{
    public class BaseRepasitory<T> : IBaseRepasitory<T> where T : class
    {
        DbContext _context;
        DbSet<T> _dbSet;
        public BaseRepasitory(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }
        public T Get(T id)
        {
            return _dbSet.Find(id);
        }
        public IQueryable<T> GetAll(Expression<Func<T, bool>> predicate)
        {
            return _dbSet.Where(predicate);
        }

        public void Remove(T item)
        {
            _dbSet.Remove(item);
            _context.SaveChanges();
        }

        public void Update(T item)
        {
            _context.Entry(item).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Add(T entity)
        {
            _dbSet.Add(entity);
            _context.SaveChanges();
        }

    }
}
