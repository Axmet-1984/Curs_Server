﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Curs_Server
{
    public interface IBaseRepasitory<T> where T:class
    {
        T Get(T id);
        IQueryable<T> GetAll(Expression<Func<T, bool>> predicate);
        void Remove(T item);
        void Update(T item);
        void Add(T entity);
    }
}
