﻿using System;

namespace Curs_Server
{
    public class Doc_Name
    {
        public Guid Id { get; set; }
        public string DocName { get; set; }
        public Guid ProfileId { get; set; }
        public Doc_Profile Profile { get; set; }

    }
}
