﻿using System;
using System.Data.Entity;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Linq;
using Curs_Server.Repasitory;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Curs_Server
{
    public class Program
    {
        static void Main(string[] args)
        {
            
            TcpListener server = null;
           
            PacientContext db = new PacientContext();
            try
            {
                server = new TcpListener(IPAddress.Parse("127.0.0.1"), 3084);
                server.Start();

                while (true)
                {
                    Console.WriteLine("Ожидание клиента");
                    TcpClient client = server.AcceptTcpClient();
                    Console.WriteLine("Клиент подключен");
                    NetworkStream stream = client.GetStream();

                    using (PacientContext pacientContext = new PacientContext())
                    {
                        var Data = pacientContext.DocNames.ToList();
                        Doc_Name[] docs = Data.ToArray();
                        
                        BinaryFormatter formatter = new BinaryFormatter();

                        // сериализуем весь массив people
                        formatter.Serialize(stream, docs);
                    }

                    //Сериализовать
                    
                    //stream.Write(data, 0, data.Length);
                   // Console.WriteLine("Команда отправлена");
                    client.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (server !=null)
                {
                    server.Stop();
                }
            }
        }
    }
}

